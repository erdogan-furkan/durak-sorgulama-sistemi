const express = require("express");
const path = require("path");
const { config } = require("dotenv");

const rootRouter = require("./routes/index");
const connectDatabase = require("./helpers/database/connectDatabase");

config({ path: "./configs/conf.env" });
const { SERVER_PORT, NODE_ENV } = process.env;
connectDatabase();

const app = express();

// Handles post requests
app.use(express.urlencoded({ extended: true }));
app.use(express.json()); // To parse the incoming requests with JSON payloads

// Template engine
app.set("view engine", "ejs");

app.use(express.static(path.join(__dirname, "static")));

// Routes
app.use(rootRouter);

app.listen(SERVER_PORT, () => {
  console.log(
    `Server started in the ${NODE_ENV} environment on PORT : ${SERVER_PORT}`
  );
});
