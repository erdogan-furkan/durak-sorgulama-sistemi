const { Router } = require("express");
const { getIndexPage } = require("../controllers/publicController");

const publicRouter = Router();

publicRouter.get("/", getIndexPage);

module.exports = publicRouter;
