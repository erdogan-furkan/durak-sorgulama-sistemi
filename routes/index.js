const { Router } = require("express");
const publicRouter = require("./publicRouter");
const apiRouter = require("./apiRouter");

const rootRouter = Router();

rootRouter.use("/", publicRouter);
rootRouter.use("/api", apiRouter);

module.exports = rootRouter;
