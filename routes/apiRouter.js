const { Router } = require("express");
const {
  getNearestStations,
  getStationsByName,
} = require("../controllers/apiController");

const apiRouter = Router();

apiRouter.post("/station", getStationsByName);
apiRouter.post("/station/nearest", getNearestStations);

module.exports = apiRouter;
