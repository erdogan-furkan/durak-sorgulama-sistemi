const getIndexPage = (req, res, next) => {
  res.render("index");
};

module.exports = {
  getIndexPage,
};
