const getNearestStations = (req, res, next) => {
  const { SPATIAL_TABLE_NAME } = process.env;
  const { lat, lon } = req.body;
  pool.query(
    `SELECT *, ST_Distance(${SPATIAL_TABLE_NAME}.geom, 'SRID=4326;POINT(${lon} ${lat})') AS dist
              FROM ${SPATIAL_TABLE_NAME}
              ORDER BY dist
              LIMIT 3;`,
    (err, result) => {
      if (err) {
        res.status(400).json({
          status: "fail",
          message: err.message,
        });
      } else {
        res.json(result.rows);
      }
    }
  );
};

const getStationsByName = (req, res, next) => {
  const { SPATIAL_TABLE_NAME } = process.env;
  pool.query(
    `SELECT DISTINCT * FROM ${SPATIAL_TABLE_NAME}
              WHERE "SDURAKADI" ILIKE '${req.body.data}%'
              ORDER BY "ILCEADI"
              LIMIT 35;`,
    (err, result) => {
      if (err) {
        res.status(400).json({
          status: "fail",
          message: err.message,
        });
      } else {
        res.json(result.rows);
      }
    }
  );
};

module.exports = {
  getNearestStations,
  getStationsByName,
};
