const { Pool } = require("pg");
const fs = require("fs");

const connectDatabase = () => {
  const {
    DB_USER,
    DB_HOST,
    DB_NAME,
    DB_PASSWORD,
    DB_PORT,
    SPATIAL_TABLE_NAME,
  } = process.env;

  // Database Connection
  global.pool = new Pool({
    user: DB_USER,
    host: DB_HOST,
    database: DB_NAME,
    password: DB_PASSWORD,
    port: DB_PORT,
  });

  const query = `
  SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename  = '${SPATIAL_TABLE_NAME}');
  CREATE EXTENSION IF NOT EXISTS postgis;
  CREATE TABLE IF NOT EXISTS "${SPATIAL_TABLE_NAME}" (
    "SDURAKKODU" int4 NOT NULL,
    "SDURAKADI" VARCHAR,
    "KOORDINAT" VARCHAR,
    "ILCEADI" VARCHAR,
    "SYON" VARCHAR,
    "AKILLI" VARCHAR,
    "FIZIKI" VARCHAR,
    "DURAK_TIPI" VARCHAR,
    "ENGELLIKULLANIM" VARCHAR,
    "geom" GEOMETRY(POINT, 4326),
    PRIMARY KEY ("SDURAKKODU")
  );`;

  pool.query(query, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      if (!result[0].rows[0].exists) {
        insertDataToDB();
      }
      console.log("Database connection is successfull.");
    }
  });
};

const insertDataToDB = () => {
  const { SPATIAL_TABLE_NAME } = process.env;
  JSON.parse(fs.readFileSync("data.json")).stations.forEach((station) => {
    pool.query(
      `INSERT INTO ${SPATIAL_TABLE_NAME} ("SDURAKKODU", "SDURAKADI", "KOORDINAT", "ILCEADI", "SYON", "AKILLI", "FIZIKI", "DURAK_TIPI", "ENGELLIKULLANIM", "geom") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`,
      [
        station.SDURAKKODU,
        station.SDURAKADI,
        station.KOORDINAT,
        station.ILCEADI,
        station.SYON,
        station.AKILLI,
        station.FIZIKI,
        station.DURAK_TIPI,
        station.ENGELLIKULLANIM,
        station.geom,
      ],
      (err) => {
        if (err) {
          console.log(err);
        }
      }
    );
  });
};

module.exports = connectDatabase;
