// Creating basemap
const map = L.map("map").setView([41.015137, 28.97953], 10);
const tileLayer = L.tileLayer(
  "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
  {
    attribution:
      '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>',
    maxZoom: 19,
  }
);
tileLayer.addTo(map);
// Create new icons
let icons = {
  redIcon: new L.Icon({
    iconUrl:
      "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png",
    shadowUrl:
      "https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
  }),
};
let tempMarker;
let tempUserMarker;
let tempString = "";

const searchStation = document.getElementById("search-station");
const btnStation = document.getElementById("btn-station");
const stationInfo = document.getElementById("stationInfo");

// Event listeners
eventListeners();
function eventListeners() {
  btnStation.addEventListener("click", htmlGeolocation);
  searchStation.addEventListener("input", postStationName);
}

async function postStationName(e) {
  if (stationInfo.innerHTML && e.target.value == "") {
    stationInfo.innerHTML = tempString;
    addFocusInteraction();
    if (tempString == "") {
      stationInfo.className = "list-group-item invisible";
    }
    return;
  } else {
    while (stationInfo.lastElementChild != undefined) {
      stationInfo.removeChild(stationInfo.lastElementChild);
    }
  }

  const response = await fetch("http://localhost:5000/api/station", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      data: e.target.value,
    }),
  });
  const stations = await response.json();

  addStationsToPanel(stations);
  addFocusInteraction();
}

function addFocusInteraction() {
  let stations = document.querySelectorAll("#station");
  stations.forEach((station) => {
    station.addEventListener("click", () => {
      let marker = L.marker(station.value.split(",").reverse()).addTo(map);
      marker.bindPopup(`${station.firstElementChild.innerHTML}`).openPopup();
      map.setView(marker.getLatLng(), 15);
      if (tempMarker !== undefined) {
        map.removeLayer(tempMarker);
      }
      tempMarker = marker;
    });
  });
}

function addStationsToPanel(stations) {
  stations.forEach((station) => {
    const akilli = station.AKILLI;
    const durak_tipi = station.DURAK_TIPI;
    const engellikullanim = station.ENGELLIKULLANIM;
    const fiziki = station.FIZIKI;
    const ilceadi = station.ILCEADI;
    const koordinat = JSON.parse(
      station.KOORDINAT.replace("POINT (", "[")
        .replace(" ", ",")
        .replace(")", "]")
    );
    const sdurakadi = station.SDURAKADI;
    const sdurakkodu = station.SDURAKKODU;
    const syon = station.SYON;
    const dist = Math.round(station.dist * 111000);

    const contents = `
    DURAK ADI: ${sdurakadi}<br>
    DURAK KODU: ${sdurakkodu}<br>
    YÖN: ${syon}<br>
    İLÇE ADI: ${ilceadi}<br>
    DURAK TİPİ: ${durak_tipi}<br>
    FİZİKİ: ${fiziki}<br>
    ENGELLİ KULLANIM: ${engellikullanim}<br>
    AKILLI: ${akilli}`;

    stationInfo.innerHTML += `
          <div id="accordion">
              <div class="card bg-light mb-2">
                  <button 
                    id="station" 
                    value="${koordinat}" 
                    class="btn btn-dark d-flex 
                    justify-content-between align-items-center" 
                    type="button">
                      <span style="display:none;">${contents}</span>
                      <span class="text-left">
                        <span class="font-weight-bold">
                          ${sdurakadi}
                        </span>
                        <br>
                        ${syon + " Yönü"}
                        <br>
                        ${ilceadi}
                      </span>
                      ${
                        dist ? `<span class="ml-auto">${dist + "m"}</span>` : ""
                      }
                      <i class="fas fa-directions ml-3"></i>
                  </button>
              </div>
          </div>
          `;
    stationInfo.className = "list-group-item overflow-auto";
  });
}

async function postUserLocation(e) {
  const response = await fetch("http://localhost:5000/api/station/nearest", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      lat: e.latlng.lat,
      lon: e.latlng.lng,
    }),
  });
  const stations = await response.json();
  return stations;
}

function addUserToMap(e) {
  let userMarker = L.marker([e.latlng.lat, e.latlng.lng], {
    icon: icons.redIcon,
  }).addTo(map);
  userMarker.bindPopup("Buradasınız!").openPopup();
  map.setView(e.latlng, 15);
  tempUserMarker = userMarker;
}

function clearPanel() {
  if (stationInfo.innerHTML) {
    while (stationInfo.lastElementChild != undefined) {
      stationInfo.removeChild(stationInfo.lastElementChild);
    }
    stationInfo.className = "list-group-item invisible";
    searchStation.value = "";
    if (tempUserMarker != undefined) {
      map.removeLayer(tempUserMarker);
    }
  }
}

function onLocationFound(e) {
  clearPanel();
  addUserToMap(e);
  postUserLocation(e).then((stations) => {
    addStationsToPanel(stations);
    addFocusInteraction();
    tempString = stationInfo.innerHTML;
    btnStation.disabled = false;
  });
}

function onLocationError(e) {
  alert(e.message);
  window.location.reload();
}

function htmlGeolocation() {
  map.locate({ enableHighAccuracy: true });
  btnStation.disabled = true;
  map.once("locationfound", onLocationFound);
  map.on("locationerror", onLocationError);
}
